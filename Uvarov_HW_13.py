import requests


# Реализовать функционал получения информации о городе посредством Python.
# User Story: При запуске файла пользователь вводит название города,
# система возвращает название города, страну, валюту и количество населения
# Tech Requirements:
# Ввод реализовать с помощью CLI интерфейса. Выбор WEB-API для получения информации - на усмотрение разработчика.
# Код должен быть читаемым, с комментариями и соответствовать принципам DRY, KISS, YAGNI.
# Кд должен быть загружен на GitHub или GitLab как отдельный проект с публичным доступом.
# Формат вывода только такой как в примере.

class Cities:
    city = ''
    country = ''
    population = 0
    currency = ''
    url1 = 'https://countriesnow.space/api/v0.1/countries/population/cities'
    url2 = 'https://countriesnow.space/api/v0.1/countries/currency'

    def __init__(self, town):
        if isinstance(town, str) and len(town) != 0:
            self.city = town
        else:
            print(f'{"-" * 25}\nSystem Error\n{"=" * 25}')

    def get_country_and_population(self):
        """ We get to know the country and population by city"""
        name = {
            "city": self.city
        }
        try:
            response = requests.request('POST', self.url1, json=name, timeout=4)
            if 200 <= response.status_code < 300:
                res = response.json()
                self.country, self.population = res['data']['country'], res['data']['populationCounts'][0]['value']
                return self.country, self.population
            else:
                print(f'Invalid city name: {self.city}')

        except Exception as ConnectTimeoutError:
            print(ConnectTimeoutError)

    def get_currency(self):
        """Using the name of the country, we pull out the currency """
        name2 = {
            "country": self.country
        }
        try:
            response2 = requests.request('POST', self.url2, json=name2, timeout=4)
            if 200 <= response2.status_code < 300:
                res2 = response2.json()
                self.currency = res2['data']["currency"]
                return self.currency
            else:
                return f'Invalid city name: {self.city}'
        except Exception as ConnectTimeoutError:
            print(ConnectTimeoutError)

    def __str__(self):
        if self.get_country_and_population() and self.get_currency():
            return f'{"-" * 25}\n{self.city}\n\n\n{self.country}\n{self.population}\n{self.currency}\n{"=" * 25}'


city = Cities('Kiev')
print(city)


